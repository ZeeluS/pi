import ConfigParser

class SocketFactory:
        def __init__(self, type = "B"):
                self.type = type
                self.connfig = ConfigParser.ConfigParser()
                self.connfig.read("SocketFactory_config.ini")
        
        def getSocket(self):
                if self.type is "B":
                        return self.__getBluetoothSocket()
                if self.type is "I":
                        return self.__getInternetSocket()
        def getSockets(self):
                return (self.__getBluetoothSocket(), self.__getInternetSocket())

        def __getBluetoothSocket(self):
                import bluetooth
                print("blue")
                port = self.connfig.get("BluetoothSocket", "port")
                s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
                s.bind(("", int(port)))
                return s

        def __getInternetSocket(self):
                print("internet")
                port = self.connfig.get("InternetSocket", "port")
                ip = self.connfig.get("InternetSocket", "ip")
                import socket
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.bind((ip, int(port)))
                return s
                


if __name__ == "__main__":
        socketFactory = SocketFactory("I")
        s = socketFactory.getSocket()
        print(s)

