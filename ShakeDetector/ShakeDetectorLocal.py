import SocketFactory
import adxl345
import threading
import time
import math
import json
import GPSDetector

class ShakeDetectorLocal(object):
    def __init__(self, maxDetectValue):
        self.maxDetectValue = maxDetectValue
        sf = SocketFactory.SocketFactory(type = "B")
        self.socket = sf.getSocket()

        self.x = 0.0
        self.y = 0.0
        self.z = 0.0

        self.adxl345 = adxl345.ADXL345()
        self.adxl345.setRange(adxl345.RANGE_16G)
        self.adxlGO = True
        self.adxlThread = threading.Thread(target=ShakeDetectorLocal.__detect, args=[self])
        self.adxlThread.start()

        self.serverGo = True

        self.gps = GPSDetector.GPSDetector()
        self.gps.start()

    def __detect(self):
        while self.adxlGO:
            axes = self.adxl345.getAxes(gforce = True)
            self.x = axes["x"]
            self.y = axes["y"]
            self.z = axes["z"]
            time.sleep(0.2)
    
    def start(self):
        self.socket.listen(1)
        while self.serverGo:
            print("Wait for client")
            clientSocket, adress = self.socket.accept()
            print("Connect to " + adress[0])
            while True:
                time.sleep(0.2)
                v = round(math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z) - 1, 3)
                if v >= self.maxDetectValue:
                    print("Detect")
                    try:
                        i = clientSocket.send(self.prepairDataToSend({'x': self.x, 'y': self.y, 'z': z}, self.gps))
                    except:
                        clientSocket.close()
    
    def prepairDataToSend(self, axis, gps):
        dist = {'axis': axis, "gps": self.gps.getCoordinateDic()}
        return json.dumps(dist)
            
    def stop(self):
        self.adxlGO =False
        self.adxlThread.join()
        self.serverGo = False
        self.gps.stop()                       

        


if __name__ == "__main__":
    shakeDetector = ShakeDetectorLocal(3)
    try:
        shakeDetector.start()
    except:
        shakeDetector.stop()
