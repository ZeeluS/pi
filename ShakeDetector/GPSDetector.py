import serial
import time
import threading
from datetime import datetime


class GPSDetector(object):

    def __init__(self):
        self.serialPort = serial.Serial ("/dev/ttyAMA0")    #Open named port 
        self.serialPort.baudrate = 9600                     #Set baud rate to 9600
        self.serialPort.timeout = None
        self.go = True

        #GPS propertis
        self.time = time.strptime("30 Nov 00", "%d %b %y")
        self.lat = 0.0
        self.lon = 0.0
        self.speed = 0.0

        self.start()

    def start(self):
        self.thread = threading.Thread(target=GPSDetector.__detect, args=[self])
        self.thread.start()

    def stop(self):
        self.go = False
        self.thread.join()
        self.serialPort.close()
    

    def __detect(self):
        while self.go:
            data = self.serialPort.readline()
            self.analizeLine(data)
            data = self.serialPort.readline()
            self.analizeLine(data)
            data = self.serialPort.readline()
            self.analizeLine(data)
            data = self.serialPort.readline()
            self.analizeLine(data)
            time.sleep(1)

    def checkSum(self, data):
        s = 0
        for c in data:
            s ^= ord(c)
        return s
    
    def analizeLine(self, line):
        if line[0] == "$":
            listOfItems = line.split(",")
            if listOfItems[0] == "$GPGGA":
                self.analizeGGA(listOfItems)
            if listOfItems[0] == "$GPRMC":
                self.analizeRMC(listOfItems)
    
    def getCoordinate(self):
        return (self.lat, self.lon)

    def getCoordinateDic(self):
        return {"lat": self.lat, "lon": self.lon, "speed": self.speed}
    
    def getAllData(self):
        return (self.lat, self.lon, self.speed)
    
    def getSpeed(self):
        return self.speed
    
    def getTime(self):
        return self.time

    def analizeGGA(self, listOfItem):
        stringToTest = ""
        hexSum = 0
        for item in listOfItem:
            if len(item) == 0:
                pass
            elif item[0] == "$":
                stringToTest += "GPGGA"
            elif item[0] == "*":
                val = item.replace("*", "")
                hexSum = int(val, 16)
            else:
                stringToTest += item

        if self.checkSum(stringToTest) == hexSum:
            #print("GGAss")
            if(listOfItem[1] != ""):
                pass
                #self.time = time.strptime(listOfItem[1], '%H%M%S.000')
            if (listOfItem[2] != "") and (listOfItem[3] != ""):
                lat = float(listOfItem[2])
                h = int(lat/100)
                m = int(lat) - h * 100
                s = int(lat*100) - h * 10000 - m*100
                templat = float(h) + float(m)/60.0 + float(s)/3600.0
                if listOfItem[3] == "S":
                    templat = templat * -1
                self.lat = templat
            if (listOfItem[4] != "") and (listOfItem[5] != ""):
                lon = float(listOfItem[4])
                h = int(lon/100)
                m = int(lon) - h * 100
                s = int(lon*100) - h * 10000 - m*100
                templon = float(h) + float(m)/60.0 + float(s)/3600.0
                if listOfItem[5] == "W":
                    templat = templat * -1
                self.lon = templon
    
    def analizeRMC(self, listOfItem):
        stringToTest = ""
        hexSum = 0
        for item in listOfItem:
            if len(item) == 0:
                pass
            elif item[0] == "$":
                stringToTest += "GPRMC"
            elif item.find("*") >= 0:
                p = item.find("*")
                first = item[0:p]
                stringToTest += first
                end = item[p:]
                val = end.replace("*", "")
                hexSum = int(val, 16)
            else:
                stringToTest += item
        
        if hexSum == self.checkSum(stringToTest):
            if listOfItem[8] != "":
                knot = float(listOfItem[7])
                self.speed = knot * 0.514444
        






if __name__ == "__main__":
    try:
        gps = GPSDetector()
        while True:
            time.sleep(1)
            #print(gps.time)
            print(gps.getAllData())
    except KeyboardInterrupt:
        gps.stop()






            