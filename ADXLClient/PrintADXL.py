import sys
from ADXLClient import *
import time

def printHelp():
    print("Program need args: ")
    print("I - internet connection")
    print("B - bluetooth connection")

def backspace(i):
    for x in range(i):
        sys.stdout.write('\r')

def line_print(string):
    for c in string:
        sys.stdout.write(c)

if __name__ == "__main__":

    if len(sys.argv) != 2:
        printHelp()
        exit()
    if (sys.argv[1] == "I" or sys.argv[1] == "B") == False:
        printHelp()
        exit()

    t = sys.argv[1]

    client = ADXLClient(t)

    try:
        client.start()
        while True:
            text = str(client.getAxesTuple())
            print(text)
            time.sleep(0.1)
    except :
        client.stop()
