import ConfigParser

class SocketFactory:
        def __init__(self, type = "I"):
                self.type = type
                self.connfig = ConfigParser.ConfigParser()
                self.connfig.read("SocketFactory_config.ini")
        
        def getSocket(self):
                if self.type is "B":
                        return self.__getBluetoothSocket()
                if self.type is "I":
                        return self.__getInternetSocket()

        def __getBluetoothSocket(self):
                import bluetooth
                print("blue")
                port = self.connfig.get("BluetoothSocket", "port")
                adres = self.connfig.get("BluetoothSocket", "adres")
                s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
                s.connect((adres, int(port)))
                return s

        def __getInternetSocket(self):
                print("internet")
                import socket
                port = self.connfig.get("InternetSocket", "port")
                host = self.connfig.get("InternetSocket", "host")
                ip = self.connfig.get("InternetSocket", "ip")
                if ip == "":
                        remote_ip = socket.gethostbyname( host )
                else:
                        remote_ip = ip
                
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((remote_ip, int(port)))
                return s
                


if __name__ == "__main__":
        socketFactory = SocketFactory("B")
        s = socketFactory.getSocket()
        print(s.recv(128))

