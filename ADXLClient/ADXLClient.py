from SocketFactory import *
from sys import platform as _platform
import json
import threading

class ADXLClient:

    def __init__(self, type = "I"):

        if _platform == "darwin":
            self.type = "I"
            print("Darwin")
        else:
            self.type = type

        self.socketFactory = SocketFactory(self.type)
        
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0

    def __downloadData(self):
        while self.__do:
            data = self.socket.recv(128)
            self.socket.send(str(len(data)))
            axieDic = json.loads(data)
            self.x = float(axieDic["x"])
            self.y = float(axieDic["y"])
            self.z = float(axieDic["z"])
            

    def start(self):
        self.__do = True
        self.socket = self.socketFactory.getSocket()
        self.thread = threading.Thread(target = ADXLClient.__downloadData, args = [self])
        self.thread.start()

    def stop(self):
        self.__do = False
        self.thread.join()
        self.socket.close()

    def getAxesTuple(self):
        return (self.x, self.y, self.z)

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getZ(self):
        return self.z


if __name__ == "__main__":
    import time
    client = ADXLClient(type = "B")
    client.start()
    for i in range(0, 100):
        print(client.getAxesTuple())
	time.sleep(0.1)
    client.stop()
        
