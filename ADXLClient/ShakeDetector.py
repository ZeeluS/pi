import sys, os, math
import time
import threading
import types
from ADXLClient import *
from  ShakeInterface import ShakeInterface

class ShakeDetector( object ):

	def __init__(self, adxl):
		
		self.adxl = adxl
		self.go = False
		self.actionList = []
		self.minimumLevelOfDetecting = 10000

	def startDetecting(self):
		self.thread = threading.Thread(target = ShakeDetector.__detectProcess, args = [self])
		self.thread.start()
		self.go = True

	def __detectProcess(self):
		self.adxl.start()
		time.sleep(0.5)
		while  self.go:
			x = ac.getX()
			y = ac.getY()
			z = ac.getZ()

			self.v = round(math.sqrt(x*x + y*y + z*z) - 1 , 3)
			if(self.v >= self.minimumLevelOfDetecting):
				self.doAction()
				time.sleep(0.5)
			time.sleep(0.2)
				

	def doAction(self):
		for i in self.actionList:
			if(i.levelOfDetecting <= self.v):
				i.doAction(self.v)


	def stop(self):
		print("Stop detect")
		self.go = False
		self.thread.join()
		self.adxl.stop()

	def addShakeAction(self, shakeAction):
		if(isinstance(shakeAction, ShakeInterface)):
			self.actionList.append(shakeAction)
			if shakeAction.levelOfDetecting < self.minimumLevelOfDetecting:
    				self.minimumLevelOfDetecting = shakeAction.levelOfDetecting
			print(str(self.minimumLevelOfDetecting))
		else:
			raise Exception() 



if __name__ == "__main__":
	type = "I"
	if(len(sys.argv) != 2):
		print("Add type of connection")
		exit()
	if(sys.argv[1] != "B" and sys.argv[1] != "I"):
		print("Unknow type of connection " + sys.argv[1])
		exit()

	ac = ADXLClient(sys.argv[1])


	shakeDetector = ShakeDetector(ac)

	class X(ShakeInterface):
		def doAction(self, shakeValeu):
			print("ShakeDetect "+ str(shakeValeu))
	x = X(3)

	class Y(ShakeInterface):
		def doAction(self, shakeValeu):
			print("ShakeDetect2 "+ str(shakeValeu))
	y = Y(5)

	shakeDetector.addShakeAction(x)
	shakeDetector.addShakeAction(y)

	shakeDetector.startDetecting()
	try:
		while True:
			time.sleep(0.5)
	except:
		shakeDetector.stop()
