from SocketFactory import *
from adxl345 import *
import sys
import threading
import json
from time import sleep
import os

class ADXL345Server:
    def __init__(self, socket):
        self.socket = socket
        self.adxl = ADXL345()
        self.adxl.setRange(RANGE_16G)

    def run(self):
        print(self.socket)
        self.socket.listen(1)
        
        while True:
            print("Wait for client")
            clientSocket, addres = self.socket.accept()
            print("Connect with " + addres[0])
            thread = threading.Thread(target = ADXL345Server.newConnectedDevice, args = [self, clientSocket])
            thread.start()
            
        self.socket.close()
    def newConnectedDevice(self, socket):
        while True:
            axes = self.adxl.getAxes(True)
            js = json.dumps(axes)
            send = socket.send(js)
            if send == 0: break
            try:
                download = int(socket.recv(128))
            except:
                print("End sending data ")
                break
            
            if send != download:
                print("error with downloading " + str(download))
                break
            sleep(0.2)
        socket.close()
            
        

if __name__ == "__main__":
    
    if len(sys.argv) == 2:
        type = sys.argv[1]
        if type == "I" or type == "B":
            socketFactory = SocketFactory(type)
            server = ADXL345Server(socketFactory.getSocket())
            server.run()
    else:
        socketFactory = SocketFactory()
        sockets = socketFactory.getSockets()
        server1 = ADXL345Server(sockets[0])
        server2 = ADXL345Server(sockets[1])
        thread1 = threading.Thread(target = ADXL345Server.run, args = [server1])
        thread2 = threading.Thread(target = ADXL345Server.run, args = [server2])
        thread1.start()
        thread2.start()
        try:
            while thread1.isAlive() and thread2.isAlive():
                thread1.join(1)
                thread2.join(1)
        except(KeyboardInterrupt, SystemExit):
            thread1.do_run = False
            thread2.do_run = False
            print("Exit program ")
            
            os.system('sudo kill %d' % os.getpid())

